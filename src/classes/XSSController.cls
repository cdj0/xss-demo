public class XSSController {
	public String Special {get;set;}
	public String AlphaNum {get;set;}
	public String Whitespace {get;set;}
	{
		Special = '< > \' " * \\ & ` ~ ! @ # $ % ^ ( ) - _ = + [ ] { } | ; : / ? . , ';
		AlphaNum = 'abcABC123';
		Whitespace = ' \t\r\n';
	}


	public String HighScoreName {get;set;}
	public Integer HighScorePoints {get;set;}

	public HighScore__c[] HighScores {get;set;}


	public void saveHighScore() {
		insert new HighScore__c(
				Score__c = HighScorePoints,
				Name__c = HighScoreName
		);
	}

	public void readHighScores() {
		HighScores = [ SELECT Name__c, Score__c FROM HighScore__c ORDER BY Score__c DESC ];
	}


	public String UserInput {get;set;}
	public String RenderSection {get;set;}

	{
		UserInput = '';
		RenderSection = '';
	}
}